# Conference Video Production



# The idea

The goal of this project is to create a reliable, hardware based, setup that can be replicated by anyone to manage video recording and/or streaming for conferences.

In addition to handling video switching and compositing in hardware, this setup is designed so that volunteers can be trained in operation within 5 minutes.

## Selecting a video switcher

Conferences actually present a fairly unique set of requirements that not many consumer level video switchers can handle. We need to have 2 video sources (presenter & slides) which are resized using DVE as well as a background image that those video sources as composited on top of.

The ATEM Mini Extreme ISO is able to handle all of this, and is the core of the setup. It also offers physical buttons which we can assign to macros that will switch views (if you want to get fancy).

## The Macros

Below is a sample set of macros that can be inserted into an ATEM mini xml config file.

MediaPlayer1 will be the overlay image, which is stored in the memory of the ATEM switcher and can be set using ATEM Software Control. Camera1, Camera2, etc. are the numbered HDMI inputs.

The macro labeled "L Slides + S Speaker" will take speaker video (camera1) reduce it in size and place it in the upper left of the screen, take slides (camera3) and place them on the right side of the screen, and take the overlay image (MediaPlayer1) and place it on top of the the whole thing. An overlay image allows you to do some fancier design, but if you prefer a simpler background instead you can just flip artAbove to "false" and that will place the image behind things.

```
    <MacroPool>
        <Macro index="0" name="L Slides + S Speaker" description="">
            <Op id="SuperSourceV2ArtFillInput" superSource="0" input="MediaPlayer1"/>
            <Op id="SuperSourceV2ArtAbove" superSource="0" artAbove="True"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="0" input="Camera1"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="0" xPosition="-10.25"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="0" yPosition="3"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="0" size="0.339996"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="1" input="Camera3"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="1" xPosition="5.60001"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="1" yPosition="2"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="1" size="0.630005"/>
            <Op id="ProgramInput" mixEffectBlockIndex="0" input="SuperSource"/>
        </Macro>
        <Macro index="1" name="L Slides + S Speaker" description="">
            <Op id="SuperSourceV2ArtFillInput" superSource="0" input="MediaPlayer1"/>
            <Op id="SuperSourceV2ArtAbove" superSource="0" artAbove="True"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="0" input="Camera2"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="0" xPosition="-10.25"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="0" yPosition="3"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="0" size="0.339996"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="1" input="Camera3"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="1" xPosition="5.60001"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="1" yPosition="2"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="1" size="0.630005"/>
            <Op id="ProgramInput" mixEffectBlockIndex="0" input="SuperSource"/>
        </Macro>
        <Macro index="2" name="L Speaker + S Slides" description="">
            <Op id="SuperSourceV2ArtFillInput" superSource="0" input="MediaPlayer1"/>
            <Op id="SuperSourceV2ArtAbove" superSource="0" artAbove="True"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="0" input="Camera3"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="0" xPosition="-10.25"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="0" yPosition="3"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="0" size="0.339996"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="1" input="Camera1"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="1" xPosition="5.60001"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="1" yPosition="2"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="1" size="0.630005"/>
            <Op id="ProgramInput" mixEffectBlockIndex="0" input="SuperSource"/>
        </Macro>
        <Macro index="3" name="L Speaker + S Slides" description="">
            <Op id="SuperSourceV2ArtFillInput" superSource="0" input="MediaPlayer1"/>
            <Op id="SuperSourceV2ArtAbove" superSource="0" artAbove="True"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="0" input="Camera3"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="0" xPosition="-10.25"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="0" yPosition="3"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="0" size="0.339996"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="1" input="Camera2"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="1" xPosition="5.60001"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="1" yPosition="2"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="1" size="0.630005"/>
            <Op id="ProgramInput" mixEffectBlockIndex="0" input="SuperSource"/>
        </Macro>
        <Macro index="4" name="L cam1 + S cam2" description="">
            <Op id="SuperSourceV2ArtFillInput" superSource="0" input="MediaPlayer1"/>
            <Op id="SuperSourceV2ArtAbove" superSource="0" artAbove="True"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="0" input="Camera1"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="0" xPosition="-10.25"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="0" yPosition="3"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="0" size="0.339996"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="1" input="Camera4"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="1" xPosition="5.60001"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="1" yPosition="2"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="1" size="0.630005"/>
            <Op id="ProgramInput" mixEffectBlockIndex="0" input="SuperSource"/>
        </Macro>
        <Macro index="5" name="L cam2 + S cam1" description="">
            <Op id="SuperSourceV2ArtFillInput" superSource="0" input="MediaPlayer1"/>
            <Op id="SuperSourceV2ArtAbove" superSource="0" artAbove="True"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="0" input="Camera2"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="0" xPosition="-10.25"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="0" yPosition="3"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="0" size="0.339996"/>
            <Op id="SuperSourceV2BoxInput" superSource="0" boxIndex="1" input="Camera4"/>
            <Op id="SuperSourceV2BoxXPosition" superSource="0" boxIndex="1" xPosition="5.60001"/>
            <Op id="SuperSourceV2BoxYPosition" superSource="0" boxIndex="1" yPosition="2"/>
            <Op id="SuperSourceV2BoxSize" superSource="0" boxIndex="1" size="0.630005"/>
            <Op id="ProgramInput" mixEffectBlockIndex="0" input="SuperSource"/>
        </Macro>
    </MacroPool>
```

## Selecting Cameras

Cameras will need to output a clean HDMI feed to work well with this setup. Additionally, since the goal is to not require specialized knowledge to operate, the cameras should be as simple to operate as the average camcorder.

The BOM includes Canon XA40 cameras, which are great, but if you wanted to cut costs you could theoretically use anything that offers a clean HDMI output. It's also worth noting here that the ATEM Mini line of switchers only supports a maximum resolution of 1080p, so there's no big advantage to going with a more expensive camera that support 8K or 16K.

## Bill of Materials
Here's the [BOM with links to vendors](https://gitlab.com/sirspamsalot/conference-video-production/-/blob/main/BOM.md) and a few extra items that might be useful.
