## Video Mixer
ATEM Mini Extreme ISO
https://www.blackmagicdesign.com/products/atemmini/techspecs/W-APS-18
### video hardware with purchase links
ATEM Mini Extreme ISO
https://www.bhphotovideo.com/c/product/1625669-REG/blackmagic_design_atem_extreme_iso_switchter.html

Canon XA40
https://www.bhphotovideo.com/c/product/1470022-REG/canon_3666c002_xa40_professional_uhd_4k.html

right angle mini HDMI to HDMI adapter
https://www.amazon.com/CableCreation-Degree-Mini-HDMI-Adapter-Support/dp/B01HQ2H2S2

SanDisk Extreme Portable SSD
https://www.bhphotovideo.com/c/product/1595431-REG/sandisk_sdssde61_1t00_g25_1tb_extreme_portable_ssd.html

Heavy duty tripod
https://www.amazon.com/dp/B0784CW9BT

### presenter video
If you need to bring presenter HDMI to the video mixer over a long distance you may want to consider an SDI/HDMI converter on both ends or an HDMI over fiber cable (fiber can be fragile, so SDI is what you usually find in event video production)

SDI/HDMI bidirectional converter
https://www.bhphotovideo.com/c/product/1674089-REG/blackmagic_design_convbdc_sdi_hdmi12g_p_micro_converter_bidirect_sdi_hdmi.html

HDMI over fiber cable (100 ft)
https://www.amazon.com/dp/B07WPVDSGN/ref=twister_B092RSKZ2W
### additional accessories
general A/V items to have on hand
- screen for monitoring video output from switcher
- 3.5mm headphones to monitor levels
- 3.5mm ground loop isolator
- HDMI cables
- 3.5mm cables
